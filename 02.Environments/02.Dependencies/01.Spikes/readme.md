## About ##

Use this folder for scripts to ensure that you can communicate with services this system is dependent on (eg IdP, MalwareDetectionService, etc.).

Common examples are simple curl scripts -- but make sure you don't embed crendentials for all to see (remember that you can use files with *.ignore or *.secret at the end to ensure that files are not committed by mistake).

### Warning ###

**NO** Environment Passwords should be embedded in any file, script, etc. in this repository.

Tip: remember that you can use files with *.ignore or *.secret at the end to ensure that files are not committed by mistake.