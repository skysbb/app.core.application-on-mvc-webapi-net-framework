## About ##

This folder contains artefacts (scripts, certs, etc.) to help setup 

* dev stations.
* target environments to deploy to.

Note that the build pipeline scripts refer to the target environment definitions.



### Warning ###

**NO** Environment Passwords should be embedded in any file, script, etc. in this repository.

Tip: remember that you can use files with *.ignore or *.secret at the end to ensure that files are not committed by mistake.