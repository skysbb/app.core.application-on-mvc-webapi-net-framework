## About ##

Doubt we'll use another environment that Azure, so it's a bit overkill 
to prepare a folder for it...but still (maybe Azure on premise will require
differences at some point)



### Warning ###

**NO** Environment Passwords should be embedded in any file, script, etc. in this repository.

Tip: remember that you can use files with *.ignore or *.secret at the end to ensure that files are not committed by mistake.