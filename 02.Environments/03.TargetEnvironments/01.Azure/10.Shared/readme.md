## About ##

This folder contains artefacts (scripts, settings) that are Common to *all* (BT,DT,ST,UT, PROD) target environments.


### Warning ###

**NO** Environment Passwords should be embedded in any file, script, etc. in this repository.

Tip: remember that you can use files with *.ignore or *.secret at the end to ensure that files are not committed by mistake.