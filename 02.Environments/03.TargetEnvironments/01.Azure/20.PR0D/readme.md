## About ##

This folder contains artefacts (scripts, settings) that related to a specific target environments.

Push as many variables as possible down to Common folder scripts.


### Warning ###

**NO** Environment Passwords should be embedded in any file, script, etc. in this repository.

Tip: remember that you can use files with *.ignore or *.secret at the end to ensure that files are not committed by mistake.