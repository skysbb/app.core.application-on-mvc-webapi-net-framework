## About ##

Use this folder for scripts to setup dev stations (eg, configure certs, etc.)



### Warning ###

**NO** Environment Passwords should be embedded in any file, script, etc. in this repository.

Tip: remember that you can use files with *.ignore or *.secret at the end to ensure that files are not committed by mistake.