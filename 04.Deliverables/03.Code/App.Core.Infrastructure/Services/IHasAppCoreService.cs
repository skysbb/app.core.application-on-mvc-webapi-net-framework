﻿namespace App.Core.Infrastructure.Services
{
    using App.Core.Shared.Services;

    public interface IHasAppCoreService : IHasAppService
    {
    }
}