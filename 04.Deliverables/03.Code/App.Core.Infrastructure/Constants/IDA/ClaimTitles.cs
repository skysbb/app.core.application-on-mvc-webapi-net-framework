﻿
namespace App.Core.Infrastructure.Constants.IDA
{
    public static class ClaimTitles
    {
        public const string ScopeElementId = "http://schemas.microsoft.com/identity/claims/scope";

        public const string CultureElementId = "http://schemas.org.tld/identity/claims/culture";

        public const string PrincipalKeyElementId = "http://schemas.org.tld/identity/claims/tenant";

        public const string ObjectIdElementId = "http://schemas.microsoft.com/identity/claims/objectidentifier";

    }
}
