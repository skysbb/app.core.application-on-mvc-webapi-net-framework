﻿
namespace App.Core.Infrastructure.Constants.KeyStores
{
    /// <summary>
    /// The names of the Keystore.
    /// These names match the KeyStore Url AppSettings:
    /// App:Core:Integration:Azure:KeyVaultStore:Organisation:Url
    /// App:Core:Integration:Azure:KeyVaultStore:Application:Url
    /// </summary>
    public static class Keystores
    {
        /// <summary>
        /// The Organisation's shared KeyStore
        /// <para>
        /// NOTE: Can be the same as the Application's
        /// if the Organisation doesn't have a shared one...
        /// </para>
        /// </summary>
        public static string Organisation = "Organisation";

        /// <summary>
        /// The System/Application's keystore.
        /// </summary>
        public static string System = "System";
    }
}
