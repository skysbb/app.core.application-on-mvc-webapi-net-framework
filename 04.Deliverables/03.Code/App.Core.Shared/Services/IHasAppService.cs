namespace App.Core.Shared.Services
{
    using App.Core.Shared.Contracts;

    public interface IHasAppService : IHasSingletonLifecycle
    {
    }
}