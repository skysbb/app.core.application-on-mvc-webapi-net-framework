namespace App.Core.Shared.Models.Messages.APIs
{
    public class SmokeTestItem
    {
        public int Id { get; set; }
        public string SomeLabel { get; set; }
    }
}