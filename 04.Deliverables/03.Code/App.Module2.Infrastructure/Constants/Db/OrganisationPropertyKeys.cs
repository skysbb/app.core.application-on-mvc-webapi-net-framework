﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Module2.Infrastructure.Constants.Db
{
    public static class OrganisationPropertyKeys
    {
        public static string OfficePhone = "OfficePhone";
        public static string Latitude = "Latitude";
        public static string Longitude = "Longitude";
    }
}

