﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Module2.Infrastructure.Constants.Db
{
    public static class AppModule2DbContextNames
    {
        // For now, only one db per Module, but could be more at some point:
        public const string Module2 = "Module2";
    }
}
