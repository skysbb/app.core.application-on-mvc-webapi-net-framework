namespace App.Module2.Shared.Models.Messages.Imports
{
    public class SchoolImportRecord
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Principal { get; set; }
        public string Website { get; set; }
        public string Street { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public string PostalAddress1 { get; set; }
        public string PostalAddress2 { get; set; }
        public string PostalAddress3 { get; set; }
        public string PostalCode { get; set; }
        public string UrbanArea { get; set; }
        public string SchoolType { get; set; }
        public string Definition { get; set; }
        public string Authority { get; set; }
        public string Gender { get; set; }
        public string TerritorialAuthoritywithAucklandLocalBoard { get; set; }
        public string RegionalCouncil { get; set; }
        public string MOELocalOffice { get; set; }
        public string EducationRegion { get; set; }
        public string GeneralElectorate { get; set; }
        public string MaoriElectorate { get; set; }
        public string CensusAreaUnit { get; set; }
        public string Ward { get; set; }
        public string COLID { get; set; }
        public string COLName { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Decile { get; set; }
        public string TotalSchoolRoll { get; set; }
        public string EuropeanPakeha { get; set; }
        public string Maori { get; set; }
        public string Pasifika { get; set; }
        public string Asian { get; set; }
        public string MELAA { get; set; }
        public string Other { get; set; }
        public string InternationalStudents { get; set; }
    }
}