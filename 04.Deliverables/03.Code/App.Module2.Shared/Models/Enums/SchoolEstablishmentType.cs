namespace App.Module2.Shared.Models.Entities
{
    public enum SchoolEstablishmentType
    {
        Undefined = 0,
        School = 1,
        Group = 2,
        COL = 4
    }
}