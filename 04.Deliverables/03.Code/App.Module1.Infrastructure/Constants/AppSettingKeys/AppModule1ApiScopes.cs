namespace App.Module1.Infrastructure.IDA.Constants.HostSettingsKeys
{
    public static class AppModule1ApiScopes
    {
        public static string ApiExampleReadScope = "example:read";
        public static string ApiExampleWriteScope = "example:write";
    }
}