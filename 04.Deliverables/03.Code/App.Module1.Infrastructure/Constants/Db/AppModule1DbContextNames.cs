﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Module1.Infrastructure.Constants.Db
{
    public static class AppModule1DbContextNames
    {
        // For now, only one db per Module, but could be more at some point:
        public const string Module1 = "Module1";
    }
}
