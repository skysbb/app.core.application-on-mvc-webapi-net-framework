﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Module2.Application.Constants.Api
{
    public static class ApiControllerNames
    {
        public static string Suffix = "";
        public static string Body = "body"+ Suffix;
        public static string EducationOrganisation = "educationorganisation" + Suffix;


        public static string SchoolAuthority = "schoolauthority" + Suffix;
        public static string SchoolDecile = "schooldecile" + Suffix;
        public static string SchoolDefinition = "schooldefinition" + Suffix;
        public static string SchoolEducationRegion = "schooleducationregion" + Suffix;
        public static string SchoolGender = "schoolgender" + Suffix;
        public static string SchoolGeneralElectorate = "schoolgeneralelectorate" + Suffix;
        public static string SchoolMaoriElectorate = "schoolmaorielectorate" + Suffix;
        public static string SchoolMinistryOfEducationLocalOffice = "schoolministryofeducationlocaloffice" + Suffix;
        public static string SchoolRegionalCouncil = "schoolregionalcouncil" + Suffix;
        public static string SchoolTerritorialAuthority = "schoolterritorialauthoritywithaucklandandlocalboard" + Suffix;
        public static string SchoolType = "schooltype" + Suffix;




    }
}
